import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const tripsApi = createApi({
    reducerPath: "trips",
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST,
    }),
    tagTypes: ['Account', 'Trip', 'Route', 'Stops'],
    endpoints: builder => ({
        token: builder.query({
            query: () => ({
                url: "/token",
                credentials: "include"
            }),
            providesTags: ["Account"]
        }),
        login: builder.mutation({
            query: ({ username, password }) => {
                const body = new FormData();
                body.append('username', username)
                body.append('password', password)
                return {
                    url: '/token',
                    method: `POST`,
                    body,
                    credentials: 'include',
                }
            },
            invalidatesTags: ['Account']
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['Account', 'Trip', 'Route', 'Stops']
        }),
        signup: builder.mutation({
            query: (data) => ({
                url: `/api/accounts`,
                method: 'POST',
                body: data,
                credentials: 'include',
            }),
            invalidatesTags: ['Account']
        }),
        getTrip: builder.query({
            query: (trip_id) => ({
                url: `/api/trips/${trip_id}`,
                credentials: 'include'
            }),
            providesTags: ['Trip'],
        }),
        getMyTrips: builder.query({
            query: () => ({
                url: '/api/trips/mine',
                credentials: 'include',
            }),
            providesTags: ['Trip'],
        }),
        createTrip: builder.mutation({
            query: (data) => ({
                url: '/api/trips',
                body: data,
                method: 'POST',
                credentials: 'include',
            }),
            invalidatesTags: ['Trip'],
        }),
        updateTrip: builder.mutation({
            query: ({ data, trip_id }) => ({
                url: `/api/trips/${trip_id}`,
                body: data,
                method: 'PUT',
                credentials: 'include',
            }),
            invalidatesTags: ['Trip'],
        }),
        deleteTrip: builder.mutation({
            query: (trip_id) => ({
                url: `/api/trips/${trip_id}`,
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['Trip'],
        }),
        addStop: builder.mutation({
            query: ({ data, trip_id }) => ({
                url: `/api/trips/${trip_id}/stop`,
                body: data,
                method: 'PUT',
                credentials: 'include',
            }),
            invalidatesTags: ['Trip', "Route"],
        }),
        removeStop: builder.mutation({
            query: ({ trip_id, data }) => ({
                url: `/api/trips/${trip_id}/stop/delete`,
                params: data,
                method: 'PUT',
                credentials: 'include',
            }),
            invalidatesTags: ['Trip', "Route"],
        }),
        getRouteData: builder.query({
            query: (trip_id) => ({
                url: `/api/${trip_id}/directions`,
                method: "POST",
                credentials: 'include',
            }),
            providesTags: ['Route'],
        }),
        getCoords: builder.mutation({
            query: (address) => ({
                url: `/api/location/${address}`,
                method: "GET",
                credentials: "include",
            }),
        }),
        getStops: builder.mutation({
            query: (data) => ({
                url: `/api/location/search`,
                method: "POST",
                body: data,
                credentials: "include",
            }),
            providesTags: ["Stops"]
        }),
    }),
});

export const {
    useTokenQuery,
    useSignupMutation,
    useLoginMutation,
    useLogoutMutation,
    useGetTripQuery,
    useGetMyTripsQuery,
    useCreateTripMutation,
    useUpdateTripMutation,
    useDeleteTripMutation,
    useAddStopMutation,
    useRemoveStopMutation,
    useGetRouteDataQuery,
    useGetCoordsMutation,
    useGetStopsMutation
} = tripsApi;
