import Login from "./components/Login";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from "./components/Nav";
import LandingPage from "./components/LandingPage";
import MyTripsList from "./components/MyTripsList";
import TestMap from "./components/maps/RouteMap";
import TripView from "./components/TripView";
import FindStops from "./components/FindStops";
import TripForm from "./components/modals/TripForm";
import SignUp from "./components/SignUp";


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/trips/mine" element={<MyTripsList />} />
        <Route path="/trips/:trip_id" element={<TripView />} />
        <Route path="/trips/:trip_id/stops" element={<FindStops />} />
        <Route path="/testmap" element={<TestMap />} />
        <Route path="/trips/mine/create" element={<TripForm />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
