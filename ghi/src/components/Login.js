import { useLoginMutation } from "../store/tripsApi";
import { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  const [login, loginResult] = useLoginMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    login({ username, password });
  };

  useEffect(() => {
    if (loginResult.isSuccess) {
      navigate("/trips/mine")
    }
  }, [loginResult, navigate])


  return (
    <div className="d-flex justify-content-center my-5 container" id="login-form">
      <div className="row col-8">
        <div className="card shadow">
          <div className="card-body">
            <form onSubmit={handleSubmit}>
              <h1 className="card-title">Login</h1>
              {loginResult.status === "rejected" && (
                <div className="alert alert-danger">
                  Invalid login credentials, please try again.
                </div>
              )}
              <p className="mb-3">Please enter your username and password.</p>
              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      type="text"
                      className="form-control"
                      id="Login__username"
                      placeholder="Username"
                      value={username}
                      required="true"
                      onChange={(e) => setUsername(e.target.value)}
                    />
                    <label htmlFor="Login__username">Username</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      type="password"
                      className="form-control"
                      id="Login__password"
                      placeholder="Password"
                      value={password}
                      required="true"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                    <label htmlFor="Login__password">Password</label>
                  </div>
                </div>
              </div>
              <div
                className="d-inline-flex justify-content-between align-items-center"
                id="login-signup-buttons"
              >
                <div>
                  <button type="submit" className="btn btn-success">
                    Login
                  </button>
                </div>
                <div className="m-3 ">
                  <Link to="/signup" className="btn btn-outline-primary">
                    <div> Don't have an account? </div>
                    <div> Click here to sign up. </div>
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}


export default Login;
