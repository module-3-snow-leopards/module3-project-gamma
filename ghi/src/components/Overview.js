import { useRemoveStopMutation } from "../store/tripsApi";
import RouteMap from "./maps/RouteMap";
import EditTripForm from "./modals/EditTripDetails";


const Overview = ({ trip, viewChange }) => {
  const [removeStop] = useRemoveStopMutation();

  const orderedStops = []
  let dayInstance = []

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {}
    const trip_id = trip.id
    data["p_id"] = e.target.value
    removeStop({ data, trip_id })
  }

  for (let i = 1; i <= trip["days"]; i++) {
    dayInstance = []
    for (let stop of trip["stops"]) {
      if (stop["day"] === i) {
        dayInstance.push(stop)
      }
    }
    orderedStops.push(dayInstance)
  }


  return (
    <div className="col-12 col-md-12 col-sm-6">
      <div className="row">
        <div className="col d-flex justify-content-center py-5" id="overview-map">
          <RouteMap trip={trip} />
        </div>
        <div className="col-lg-6 container-fluid d-flex justify-content-center" id="center-stop-list">
          <div className="col-lg-6 justify-content-center" id="width-stop-list">
            <div className="pb-4" id="name-description">
              <h1>{trip.name} | {trip.days} Days <button
                type="button"
                className="btn btn-success m-2" data-bs-toggle="modal" data-bs-target="#edit-form">✏️</button></h1>
              <div className="modal fade" id="edit-form">
                {trip && (
                  <EditTripForm trip={trip} />
                )}
              </div>
              {trip.description}
            </div>
            <div id="stop-list">
              {orderedStops.map((day, index) => (
                <div key={index} className="d-grid" id="stop-dropdown">
                  <button className="btn btn-outline-dark d-inline-flex text-center my-2"
                    id="dropdownMenuButton"
                    data-bs-toggle="collapse"
                    data-bs-auto-close="false"
                    data-bs-target={`#stop-menu-${index}`}
                    aria-expanded="false">Day: {index + 1}</button>
                  <div className="collapse" id={`stop-menu-${index}`}>
                    {day.length === 0 && (
                      <div className="text-center">
                        <p>There are no stops for this day.</p>
                        <button value="find" onClick={viewChange} className="btn btn-success">Add a stop</button>
                      </div>
                    )}
                    {day.map((stop) => (
                      <div className="card ms-0" id="stop-card">
                        <div className="card-body">
                          <h5 className="card-title">{stop.stop_name}</h5>
                          <p></p>
                          <p>
                            Address:{" "}
                            {stop.address}
                          </p>
                          <p>
                            Coordinates:{" "}
                            {`longitude: ${stop.coordinates.longitude} | latitude: ${stop.coordinates.latitude}`}
                          </p>
                          <p>
                            Duration: {stop.duration}

                          </p>
                        </div>
                        <div className="card-footer">
                          <div className="float-end">
                            <button
                              className="btn btn-outline-danger"
                              value={stop["p_id"]}
                              onClick={handleSubmit}
                            >Remove Stop
                            </button>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default Overview;
