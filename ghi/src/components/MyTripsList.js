import { useGetMyTripsQuery, useDeleteTripMutation, useTokenQuery } from "../store/tripsApi"
import { Link } from "react-router-dom";
import TripForm from "./modals/TripForm";

function MyTripsList() {
  const { data: trips, isLoading } = useGetMyTripsQuery();
  const [deleteTrip] = useDeleteTripMutation();
  const { data: account } = useTokenQuery();

  const handleSubmit = (e) => {
    e.preventDefault();
    const trip_id = e.target.value
    deleteTrip(trip_id)
  }

  return (
    <div className="container-fluid">
      <h1 className="header-line mx-5 my-5" id="header">My Trips <button className="btn btn-outline-success m-3" data-bs-toggle="modal" data-bs-target="#createModal">+ Create Trip</button></h1>
      <div className="modal fade" id="createModal">
        {!isLoading && (<TripForm user={account.account.username} />)}
      </div>
      {!account && (
        <div className="position-absolute top-50 start-50 translate-middle">
          <h1>You are not logged in.</h1>
          <Link to="/login" className="btn btn-primary m-3">
            <h1>Click here to login</h1>
          </Link>
        </div>
      )}
      <div className="row align-items start justify-contents-center">
        {!isLoading && trips.trips.map(trip => {
          return (
            <div key={trip.id} className="col-12 col-md-4 p-4">
              <div className="card card-style shadow">
                <img alt="a-mountainscape-with-trees"
                  src="https://images.unsplash.com/photo-1610878180933-123728745d22?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1074&q=80"
                  className="card-img-top"
                />
                <div className="card-body">
                  <h5 className="card-title">{trip.name}</h5>
                  <p className="card-text">{trip.description}</p>
                  <Link
                    to={`/trips/${trip.id}`}
                    className={"btn btn-success"}
                  >
                    Open in TripView
                  </Link>
                  <button
                    value={trip["id"]}
                    onClick={handleSubmit}
                    className="btn btn-danger mx-3"
                  >
                    Delete
                  </button>
                  <label className="icons">
                    {trip.is_public ? "🌐" : "🔒"}
                  </label>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default MyTripsList;
