import React, { useState } from "react";
import { useCreateTripMutation } from "../../store/tripsApi";
import { useNavigate } from "react-router";

const TripForm = ({ user }) => {
  const navigate = useNavigate();

  const [createTrip] = useCreateTripMutation();

  const [name, setName] = useState("")
  const [description, setDescription] = useState("")
  const [days, setDays] = useState(1)

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {}
    data["name"] = name
    data["author"] = user
    data["description"] = description
    data["is_public"] = false
    data["days"] = days
    data["is_completed"] = false
    data["rating"] = 0
    data["stops"] = []
    createTrip(data)
    navigate(`/trips/mine`)
  }

  return (
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Create a Trip!</h5>
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div className="modal-body">
          <form onSubmit={handleSubmit}>
            <div>
              <label className="form-floating mb-3">Name:
                <input value={name} className="form-control" minLength={2} htmlname="name" placeholder="Name" required={true} onChange={(e) => {
                  setName(e.target.value);
                }} />
              </label>
            </div>
            <div>
              <label className="form-floating mb-3">Description:
                <textarea required="true" value={description} minLength={2} className="form-control" htmlname="description" type="textarea" onChange={(e) => {
                  setDescription(e.target.value);
                }} />
              </label>
            </div>
            <div>
              <label className="form-floating mb-3">Days:
                <input value={days} className="form-control" type="number" min="1" htmlname="days" placeholder="Description" required={true} onChange={(e) => {
                  setDays(e.target.value);
                }} />
              </label>
            </div>
          </form>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={handleSubmit}>Save changes</button>
        </div>
      </div>
    </div>
  );
}

export default TripForm;
