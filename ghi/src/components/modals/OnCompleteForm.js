import React, { useState } from "react";
import { useUpdateTripMutation } from "../../store/tripsApi";
import { useParams } from "react-router";


const OnCompleteForm = ({ name, author, description, days, stops }) => {
    const { trip_id } = useParams();

    const [completeTrip] = useUpdateTripMutation();

    const [isPublic, setIsPublic] = useState(false);
    const [rating, setRating] = useState(1)

    const handleSubmit = (e) => {
        e.preventDefault();
        const data = {}
        data["name"] = name
        data["author"] = author
        data["description"] = description
        data["is_public"] = isPublic
        data["days"] = days
        data["is_completed"] = true
        data["rating"] = rating
        data["stops"] = stops
        completeTrip({ data, trip_id })
    }

    return (
        <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">Complete Your Trip!</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <form key={trip_id} onSubmit={handleSubmit}>
                        <div>
                            <label className="form-floating mb-3" htmlFor="is_public">Make Public?
                                <input type="checkbox" name="is_public" onChange={(e) => {
                                    setIsPublic(e.target.checked);
                                }} />
                            </label>
                        </div>

                        <div className="form-floating mb-3">
                            <input placeholder="rating" name="rating" onChange={(e) => {
                                setRating(e.target.value);
                            }} value={rating} className="form-control" />
                            <label htmlFor="rating">Rating: </label>
                        </div>

                    </form>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={handleSubmit}>Save changes</button>
                </div>
            </div>
        </div>
    )
};

export default OnCompleteForm;
