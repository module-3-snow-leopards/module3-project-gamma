import React, { useState } from "react";
import { useUpdateTripMutation } from "../../store/tripsApi";
import { useParams } from "react-router";

const EditTripForm = ({ trip }) => {
    const { trip_id } = useParams();

    const [editTrip] = useUpdateTripMutation();

    const [name, setName] = useState(trip.name);
    const [description, setDescription] = useState(trip.description);
    const [days, setDays] = useState(trip.days)


    const handleSubmit = (e) => {
        e.preventDefault();
        const data = {}
        const adjustedStops = []

        for (let stop of trip.stops) {
            if (stop.day <= days) {
                adjustedStops.push(stop)
            }
        }

        data["name"] = name
        data["author"] = trip.author
        data["description"] = description
        data["is_public"] = trip.is_public
        data["days"] = days
        data["is_completed"] = trip.is_completed
        data["rating"] = trip.rating
        data["stops"] = adjustedStops

        editTrip({ data, trip_id })
    }


    return (
        <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">Edit Trip Details</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <form key={trip.id} onSubmit={handleSubmit}>


                        <label className="form-floating mb-3" >Title
                            <input required="true" placeholder={name} value={name} onChange={(e) => { setName(e.target.value) }} className="form-control" />
                        </label>

                        <label className="form-floating mb-3" >Description
                            <textarea required="true" placeholder={description} value={description} onChange={(e) => { setDescription(e.target.value) }} className="form-control" />
                        </label>

                        <label className="form-floating mb-3" >Days
                            <input required="true" type="number" min={1} placeholder={days} onChange={(e) => { setDays(e.target.value) }} value={days} />
                            {trip.days > days && (
                                <p>Warning: Reducing the number of days will remove all stops taken after the new day</p>
                            )}
                        </label>
                    </form>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={handleSubmit}>Save changes</button>
                </div>
            </div>
        </div>
    )
}

export default EditTripForm;
