import React, { useState, useEffect } from "react";
import { useAddStopMutation } from "../../store/tripsApi";
import { useParams } from "react-router";


const StopForm = ({ text, placeName, index, days }) => {
    days = days.days
    const { trip_id } = useParams();

    const [addStop, stopResponse] = useAddStopMutation();

    const [addedSuccess, setAddedSuccess] = useState(false)

    const [duration, setDuration] = useState(0)
    const [day, setDay] = useState(1)

    const handleSubmit = (e) => {
        e.preventDefault();
        const data = {}
        data["stop_name"] = text
        data["address"] = placeName
        data["duration"] = duration
        data["day"] = day
        addStop({ data, trip_id })
    }

    const spliceAddress = () => {
        let firstChar = Number(placeName[0])
        if (!Object.is(firstChar, NaN)) {
            return placeName
        } else {
            return placeName.slice(text.length + 2)
        }
    }

    useEffect(() => {
        if (stopResponse.isSuccess) {
            setAddedSuccess(true)
        }
    }, [stopResponse])

    return (
        <div>
            {addedSuccess && (
                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                    Successfully Added Stop!
                </div>
            )}
            {!addedSuccess && (
                <form key={index} className="card" onSubmit={handleSubmit}>
                    <input placeholder={text} name="name" value={text} readOnly={true}>
                    </input>

                    <input placeholder={placeName} name="address" value={spliceAddress()} readOnly={true}>
                    </input>

                    <label> Duration {`(in minutes)`}:
                        <input required="true" placeholder="duration" type="number" step={15} min={0} name="duration" onChange={(e) => {
                            setDuration(e.target.value);
                        }} defaultValue={duration}>
                        </input>
                    </label>

                    <label> Day:
                        <input type="number" max={days} min={1} required={true} placeholder="day" name="day" onChange={(e) => {
                            setDay(e.target.value);
                        }} value={day}>
                        </input>
                    </label>

                    <button className={"btn btn-success"}>Add To Trip</button>
                </form>
            )}
        </div>
    )
}

export default StopForm;
