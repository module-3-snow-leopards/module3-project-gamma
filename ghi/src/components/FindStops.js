import StopMap from "./maps/StopMap";
import { useGetCoordsMutation, useGetStopsMutation } from "../store/tripsApi";
import { useEffect, useState } from "react";
import StopForm from "./modals/StopForm";

const FindStops = (days) => {
  const [address, setAddress] = useState("")
  const [keyword, setKeyword] = useState("")

  const [coordinates, coordinateResponse] = useGetCoordsMutation();
  const [getStops, stopListResponse] = useGetStopsMutation();

  const handleAddressSubmit = (e) => {
    e.preventDefault();
    setKeyword(e.target.value)
    coordinates(address);
  }


  useEffect(() => {
    if (coordinateResponse.isSuccess) {
      const stopRequest = {
        "key_word": keyword,
        "longitude": coordinateResponse.data["longitude"],
        "latitude": coordinateResponse.data["latitude"],
      }
      getStops(stopRequest)
    }
  }, [coordinateResponse, getStops, keyword])


  return (
    <div className="col-12 col-md-12 col-sm-6">
      <div className="row">
        <div className="col d-flex justify-content-center py-5" id="overview-map">
          <StopMap stopListData={stopListResponse.data} />
        </div>
        <div className="col-lg-6 container-fluid justify-content-center py-5" id="find-stop-container">
          <form onSubmit={handleAddressSubmit}>
            <div className="input-group mb-2" id="stop-search">
              <input
                className="form-control shadow-sm"
                placeholder="address"
                onChange={(e) => {
                  setAddress(e.target.value);
                }}
                value={address}
              ></input>
              <button className="btn btn-outline-success shadow-sm" onClick={handleAddressSubmit} value={address}>Find a stop</button>
            </div>
            <div id="keyword-buttons-div">
              <button className="btn mx-3 my-4 shadow" id="keyword-button" onClick={handleAddressSubmit} value={"fast food"}>🍕</button>
              <button className="btn mx-3 my-4 shadow" id="keyword-button" onClick={handleAddressSubmit} value={"Fuel"}>⛽</button>
              <button className="btn mx-3 my-4 shadow" id="keyword-button" onClick={handleAddressSubmit} value={"Hotel"}>🛌</button>
              <button className="btn mx-3 my-4 shadow" id="keyword-button" onClick={handleAddressSubmit} value={"Landmark"}>🗽</button>
              <button className="btn mx-3 my-4 shadow" id="keyword-button" onClick={handleAddressSubmit} value={"Airport"}>✈️</button>
            </div>
          </form>
          <div id="find-stop-results">
            <div>
              {stopListResponse.isSuccess &&
                stopListResponse.data.places.map((stop, index) => (
                  <div key={index}>
                    <StopForm days={days} text={stop.text} placeName={stop.place_name} index={index} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FindStops;
