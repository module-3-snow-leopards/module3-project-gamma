import React from "react";
import { Link } from "react-router-dom";
import TripClip from "../TripClip.mp4";
import { useTokenQuery } from "../store/tripsApi";
import LogoWhite from "../LogoWhite.svg"

function LandingPage() {
  const { data: account } = useTokenQuery();

  return (
    <>
      <div>
        <div style={{ position: 'absolute', top: '0', bottom: '0', width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.2', zIndex: '-1' }} className='overlay'></div>
        <video autoPlay loop muted className="video" style={{ zIndex: '-2' }}>
          <source src={TripClip} type="video/mp4" />
        </video>
      </div>
      <div className="container justify-content-center" id="landing-page">
        <div className="container justify-content-center">
          <img src={LogoWhite} style={{ width: '60%', height: '60%' }} id="triphub-logo" alt="landing-page-logo"></img>
        </div>
        <div>
          <h1 className="title font d-flex">
            {account && (
              <Link
                to={"/trips/mine"}
                className="drk-btn btn-bigger">
                Are you ready to plan your next adventure?
              </Link>
            )}
            {!account && (
              <Link
                to={"/login"}
                className="drk-btn btn-bigger">
                Are you ready to plan your next adventure?
              </Link>
            )}
          </h1>
        </div>
      </div>
    </>
  );
}

export default LandingPage;
