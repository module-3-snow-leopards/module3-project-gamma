import { useGetTripQuery, useGetRouteDataQuery } from "../store/tripsApi";
import { useParams } from "react-router";
import FindStops from "./FindStops";
import OnCompleteForm from "./modals/OnCompleteForm";
import Overview from "./Overview";
import { useState } from "react";
import TripFooter from "./TripFooter";


function TripView() {
  const { trip_id } = useParams()
  const { data: trip, isLoading } = useGetTripQuery(trip_id);
  const { data: directions } = useGetRouteDataQuery(trip_id);

  const [viewState, setViewState] = useState(true)

  const handleViewChange = (e) => {
    e.preventDefault();
    if (e.target.value === "find") {
      setViewState(false)
    } else {
      setViewState(true)
    }
  }


  if (isLoading) {
    return <div>This may take a while</div>;
  }
  if (!trip) {
    return <div>No trip found!</div>;
  }


  return (
    <div className="container-fluid">
      <div className="nav nav-tabs d-flex justify-content-center mx-4 my-3" id="find-overview-toggle">
        <button className="btn" value="find" id={!viewState && ("find-overview-togglers-active")} onClick={handleViewChange} >Find</button>
        <button className="btn" value="overview" id={viewState && ("find-overview-togglers-active")} onClick={handleViewChange} >Overview</button>
      </div>
      {!viewState && (
        <FindStops days={trip.days} className="flex-child-element" />
      )}
      {viewState && !isLoading && trip && (
        <Overview viewChange={handleViewChange} trip={trip} className="flex-child-element" />
      )}
      <div id="footer-div">
        <div className="row">
          <div className="col-10">
            {directions && trip && (<TripFooter stops={trip.stops} duration={directions.duration} />)}
          </div>
          <button className="col-1 btn btn-success m-4" data-bs-toggle="modal" data-bs-target="#on-complete-form">Complete Trip</button>
        </div>
      </div>
      <div className="modal fade" id="on-complete-form">
        <OnCompleteForm name={trip.name} author={trip.author} description={trip.description} days={trip.days} stops={trip.stops} />
      </div>
    </div>
  );
}

export default TripView;
