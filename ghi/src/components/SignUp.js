import { useSignupMutation } from "../store/tripsApi";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";


const SignUp = () => {
  const navigate = useNavigate();
  const [signup, signupResponse] = useSignupMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [full_name, setFullName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    signup({
      "username": username,
      "email": email,
      "password": password,
      "full_name": full_name
    });
  };

  useEffect(() => {
    if (signupResponse.isSuccess) {
      navigate("/trips/mine")
    }
  }, [signupResponse, navigate])


  return (
    <div className="d-flex justify-content-center my-5 container">
      <div className="col-6">
        <div className="card shadow">
          <div className="card-body">
            <form onSubmit={handleSubmit}>
              <h1 className="card-title">Sign Up</h1>
              {signupResponse.isLoading && <div className="alert alert-success">Creating account...</div>}
              <p className="mb-3">Please fill in your information below.</p>
              {signupResponse.status === "rejected" && (
                <div className="alert alert-danger">
                  Could not create an account with those credentials, please try again.
                </div>
              )}
              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      type="text"
                      className="form-control"
                      id="username"
                      placeholder="Username"
                      value={username}
                      required="true"
                      onChange={(e) => setUsername(e.target.value)}
                    />
                    <label htmlFor="username">Username</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      type="text"
                      className="form-control"
                      id="email"
                      placeholder="Email"
                      value={email}
                      required="true"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <label htmlFor="email">Email</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      type="text"
                      className="form-control"
                      id="fullName"
                      placeholder="Full Name"
                      value={full_name}
                      required="true"
                      onChange={(e) => setFullName(e.target.value)}
                    />
                    <label htmlFor="fullName">Full Name</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      type="password"
                      className="form-control"
                      id="password"
                      placeholder="Password"
                      required="true"
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                    />
                    <label htmlFor="password">Password</label>
                  </div>
                </div>
              </div>

              <div
                className="d-inline-flex justify-content-between align-items-center"
                id="login-signup-buttons"
              >
                <div>
                  <button type="submit" className="btn btn-success">
                    Sign Up
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};


export default SignUp;
