import { NavLink, useNavigate } from "react-router-dom";
import { useTokenQuery, useLogoutMutation } from "../store/tripsApi";
import userIcon from "../user-icon.svg";
import LogoWhite from "../LogoWhite.svg"


const Nav = () => {
  const { data: account } = useTokenQuery();
  const navigate = useNavigate()
  const [logout] = useLogoutMutation();

  const HandleLogout = (e) => {
    e.preventDefault();
    logout();
    navigate("/")
  }


  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" id="navbar-styles">
      <div className="container-fluid">
        <NavLink to="/" className="navbar-brand">
          <img src={LogoWhite} style={{ width: '160px', height: '50px' }} alt="nav-logo"></img>
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-column" id="navbarSupportedContent">
          {!account && (
            <div className="justify-content-md-end">
              <button className="btn btn-outline-light me-md-2">
                <NavLink to={"/login"} id="account-links">
                  Login
                </NavLink>
              </button>
              <button className="btn btn-outline-light">
                <NavLink to={"/signup"} id="account-links">
                  Sign Up
                </NavLink>
              </button>
            </div>
          )}
          {account && (
            <div className="dropdown ms-auto" id="user-dropdown">
              <button
                className="btn btn-outline-light dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img className="user-icon" src={userIcon} alt="User Icon" />
                {" "}{account.account.username}{" "}
              </button>
              <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                <li>
                  <button className="dropdown-item btn btn-danger" onClick={HandleLogout}>
                    Logout
                  </button>
                </li>
              </ul>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Nav;
