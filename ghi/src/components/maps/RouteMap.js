import React, { useState, useEffect } from "react";
import MapGL, { Marker, Source, Layer } from "react-map-gl";
import { useGetRouteDataQuery } from "../../store/tripsApi";
import "mapbox-gl/dist/mapbox-gl.css";
import mapboxgl from 'mapbox-gl';

// @ts-ignore
// eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_TOKEN

const RouteMap = ({ trip }) => {
  const routeResponse = useGetRouteDataQuery(trip.id);
  const [lineData, setLineData] = useState({})

  const [viewport, setViewport] = useState({
    latitude: 39.5,
    longitude: -98.35,
    zoom: 3,
  });

  useEffect(() => {
    if (routeResponse.isSuccess) {
      setLineData(
        {
          type: "Feature",
          geometry: routeResponse.data.geometry,
        }
      )
    }
  }, [routeResponse])

  const layerStyle = {
    id: "route",
    type: "line",
    source: "route",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "green",
      "line-width": 3,
    }
  }

  return (
    <div className="map-viewport">
      <MapGL
        {...viewport}
        onMove={(evt) => setViewport(evt.viewState)}
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxAccessToken={mapboxgl.accessToken}
      >
        {trip && (
          trip.stops.map((stop, stopIndex) => (
            <Marker
              color="red"
              key={`${trip.id}-${stopIndex}`}
              longitude={stop.coordinates.longitude}
              latitude={stop.coordinates.latitude}
            >
            </Marker>
          ))
        )}
        {routeResponse.isSuccess && (
          <Source id="route" type="geojson" data={lineData}>
            <Layer {...layerStyle} />
          </Source>
        )}
      </MapGL>
    </div>
  );
}

export default RouteMap;
