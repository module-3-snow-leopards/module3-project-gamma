import React, { useState, useEffect } from "react";
import MapGL, { Marker } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import mapboxgl from 'mapbox-gl';

// @ts-ignore
// eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_TOKEN

function StopMap({ stopListData }) {
  const [viewport, setViewport] = useState({
    latitude: 39.5,
    longitude: -98.35,
    zoom: 3,
  });

  useEffect(() => {
    if (stopListData && stopListData.places && stopListData.places.length > 0) {
      setViewport({
        latitude: stopListData.places[0].latitude,
        longitude: stopListData.places[0].longitude,
        zoom: 5,
      });
    }
  }, [stopListData]);

  return (
    <div className="map-viewport">
      <MapGL
        {...viewport}
        onMove={(evt) => setViewport(evt.viewState)}
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxAccessToken={mapboxgl.accessToken}
      >
        {stopListData &&
          stopListData.places &&
          stopListData.places.map((place, index) => (
            <Marker
              color="red"
              key={index}
              longitude={place.longitude}
              latitude={place.latitude}
            >
            </Marker>
          ))}

      </MapGL>
    </div>
  );
}

export default StopMap;
