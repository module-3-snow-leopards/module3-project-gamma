import { useParams } from "react-router";


const TripFooter = ({ stops, duration, distance }) => {
    const { trip_id } = useParams();

    let routeDurationInMins = Math.ceil((duration / 60))


    function convertMinutes(minutes) {
        const hours = Math.floor(minutes / 60);
        const mins = minutes % 60;
        return `${hours} hours ${mins} min`;
    }


    let totalStopDuration = 0
    if (duration && stops) {
        for (let stop of stops) {
            totalStopDuration += stop.duration
        }
    }

    return (
        <div>
            <table key='tripTimes' className='table table-borderless'>
                <thead>
                    <tr>
                        <th> Total Stop Duration: </th>
                        <th> Total Driving Time: </th>
                        <th> Total Trip Time: </th>
                    </tr>
                </thead>
                <tbody>
                    {stops && duration && (
                        <tr key={trip_id} id="footer-content">
                            <td>{convertMinutes(totalStopDuration)}</td>
                            <td>{convertMinutes(routeDurationInMins)}</td>
                            <td>{convertMinutes(totalStopDuration += routeDurationInMins)}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default TripFooter
