# TripHub
***

## Authored by:
* Noah
* Dane
* Mason

# Design
See docs/diagram.png

# Integrations
This application requires GeoCoding and Direcitons data from Mapbox API (third-party)

## Functionality
TripHub is designed to make planning a road trip simple and efficient. A user is first directed to a landing page where they are asked if they are ready to start an adventure. If so, they can click on that message and be directed to a login page. Besides logging the user in, the page also gives the user the ability to be redirected to a sign-up page where they can register an account. Once logged in, the user is then presented with the list of trips that they have planned. On the MyTripList page, the user can easily create a new trip by clicking on the "Create Trip" button. This brings up a popup that provides the user with a form that has three fields that will specify the trip name, a short description of the trip, and the length of the trip in days. On completion of this form, a trip card will populate on the MyTripList page. The card has two buttons, one to add stops to the trip and one to delete the trip. The delete button permanently deletes the trip from the trip list. The add stops button directs the user to an overview page. This overview page contains two major elements. The first is the map and the second is the stop list. The stop list will be populated with however many days there are on that given trip. To add a stop, the user simply needs to click on one of the days which will then show a button that says "add stop". Clicking this button sends the user to the FindStops page. On the FindStops page, there is a search bar where a user can input a location that they are interested in. On search of the location, the map on the page will be populated with 10 marked locations that meet the search requirement. On search, there will also be a list of all of the stops with their name and address along with 2 input fields, duration and day. The duration field specifies how long the user plans to stay at that particular stop and the day field specifies what day the user plans to stop at that location. At the bottom of each stop location, there is a button that adds the stop to the stop list in the overview page. When the user then navigates over to the overview page, the map will be populated with the stops that had been added from the find stop page along with a route to all of the stops.

## Project Initialization
To run this application on your local machine follow these steps:
- Clone the repository down to your local machine
- CD into the new project directory
- Run docker volume create triphub-data
- Set the signing key inside the .env as SIGNING_KEY
- Go to https://docs.mapbox.com/api/overview/ to get a mapbox api token and put it in your .env as REACT_APP_MAPBOX_TOKEN
- Run docker compose build
- Run docker compose up
- Enjoy!
