from pydantic import BaseModel
from typing import Optional, List


class DeleteStatus(BaseModel):
    deleted: bool


class CoordsIn(BaseModel):
    latitude: float = 0
    longitude: float = 0


class StopIn(BaseModel):
    stop_name: str
    address: str
    duration: int
    day: int


class StopOut(BaseModel):
    stop_name: str
    address: str
    duration: int
    day: int
    coordinates: CoordsIn
    p_id: int = 0


class TripIn(BaseModel):
    name: str
    author: str
    description: str
    is_public: bool = False
    days: int
    is_completed: bool = False
    rating: int = 0
    stops: Optional[List[StopOut]] = None


class TripOut(BaseModel):
    id: str
    name: str
    author: str
    description: str
    is_public: bool = False
    days: int
    is_completed: bool = False
    rating: int
    stops: Optional[List[StopOut]] = None


class TripsList(BaseModel):
    trips: list[TripOut]
