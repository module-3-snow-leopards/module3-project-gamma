from pydantic import BaseModel
from typing import List


class RouteIn(BaseModel):
    start_longitude: float
    start_latitude: float
    end_longitude: float
    end_latitude: float


class Geometry(BaseModel):
    cooridenates: List[List[float]]


class Route(BaseModel):
    distance: float
    duration: float


class GeocodeIn(BaseModel):
    latitude: float
    longitude: float


class GeocodeOut(BaseModel):
    latitude: float
    longitude: float


class SearchIn(BaseModel):
    key_word: str
    longitude: float
    latitude: float


class SearchOut(BaseModel):
    place_name: str
    text: str
    longitude: float
    latitude: float


class SearchOutList(BaseModel):
    places: list[SearchOut]


class RouteOut(BaseModel):
    waypoints: list
    geometry: dict
    duration: float
    distance: float
