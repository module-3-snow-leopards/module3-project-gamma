from queries.client import MongoQueries
from bson.objectid import ObjectId
from models.trips import TripIn


class TripQueries(MongoQueries):
    collection_name = "trips"

    def get_all_trips(self):
        results = []
        for trip in self.collection.find():
            trip["id"] = str(trip["_id"])
            if "stops" not in trip:
                trip["stops"] = {"stops": []}
            results.append(trip)
        return results

    def get_user_trips(self, email: str):
        results = []
        for trip in self.collection.find({"author": email}):
            trip["id"] = str(trip["_id"])
            if "stops" not in trip:
                trip["stops"] = {"stops": []}
            results.append(trip)
        return results

    def get_one_by_id(self, trip_id: str):
        trip = self.collection.find_one({"_id": ObjectId(trip_id)})
        if trip is None:
            return None
        trip["id"] = str(trip["_id"])
        return trip

    def create(self, trip_in: TripIn, author):
        trip_in = dict(trip_in)
        trip_in["author"] = author
        self.collection.insert_one(trip_in)
        trip_in["id"] = str(trip_in["_id"])
        return trip_in

    def update(self, trip_id: str, trip_in: TripIn, author):
        trip = dict(trip_in)
        trip["author"] = author
        stops_array = []
        if len(trip["stops"]) > 0:
            for stop in trip["stops"]:
                stops_array.append(dict(stop))
            for stop_dict in stops_array:
                stop_dict["coordinates"] = dict(stop_dict["coordinates"])
        trip["stops"] = stops_array
        result = self.collection.update_one(
            {"_id": ObjectId(trip_id)}, {"$set": trip}
        )
        if result.matched_count == 0:
            return None
        trip["id"] = trip_id
        return trip

    def delete(self, trip_id: str):
        result = self.collection.delete_one({"_id": ObjectId(trip_id)})
        return result.deleted_count > 0
