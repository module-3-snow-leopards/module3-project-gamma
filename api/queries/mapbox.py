import requests
import os
import uuid


mapbox_token = os.environ.get("REACT_APP_MAPBOX_TOKEN")


class MapboxQueries:
    def geocode(self, address: str):
        response = requests.get(
            f"https://api.mapbox.com/geocoding/v5/mapbox.places/{address}.json",  # noqa
            params={"access_token": mapbox_token},
        )
        data = response.json()

        latitude = data["features"][0]["center"][1]
        longitude = data["features"][0]["center"][0]
        result = {"latitude": latitude, "longitude": longitude}

        return result

    def search_location(self, dict_of_values):
        longitude = dict_of_values["longitude"]
        latitude = dict_of_values["latitude"]
        keyword = dict_of_values["key_word"]
        limit = 10
        mapbox_url = f"https://api.mapbox.com/geocoding/v5/mapbox.places/{keyword}.json?limit={limit}&access_token={mapbox_token}"  # noqa
        session_token = uuid.uuid4()
        response = requests.get(
            mapbox_url,
            params={
                "session_token": session_token,
                "proximity": [longitude, latitude],
            },
        )
        data = response.json()
        return data

    def get_directions(self, trip):
        stops = trip["stops"]
        mapbox_url = (
            f"https://api.mapbox.com/directions/v5/mapbox/driving/"  # noqa
        )
        ordered_stops = []
        for day in range(1, trip["days"] + 1):
            for stop in stops:
                if stop["day"] == day:
                    ordered_stops.append(stop)

        for stop in ordered_stops:
            mapbox_url += (
                str(stop["coordinates"]["longitude"])
                + "%2C"
                + str(stop["coordinates"]["latitude"])
                + "%3B"
            )
        mapbox_url = mapbox_url[:-3]
        response = requests.get(
            mapbox_url,
            params={
                "geometries": "geojson",
                "language": "en",
                "overview": "full",
                "steps": "false",
                "access_token": mapbox_token,
            },
        )
        result = response.json()
        route = {}
        route["waypoints"] = result["waypoints"]
        route["geometry"] = result["routes"][0]["geometry"]
        route["distance"] = result["routes"][0]["distance"]
        route["duration"] = result["routes"][0]["duration"]
        return route
