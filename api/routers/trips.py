from fastapi import APIRouter, Depends, HTTPException
from authenticator import authenticator
from typing import Optional
from models.trips import TripIn, TripOut, TripsList, DeleteStatus, StopIn
from queries.trips import TripQueries
from queries.mapbox import MapboxQueries

router = APIRouter()


@router.post("/api/trips", response_model=TripOut)
def create_trip(
    trip_in: TripIn,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    if account_data:
        return queries.create(trip_in, author=account_data["username"])
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )


@router.get("/api/trips", response_model=TripsList)
def list_all_trips(
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    if account_data:
        return {"trips": queries.get_all_trips()}
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )


@router.get("/api/trips/mine", response_model=TripsList)
def list_user_trips(
    account_data: Optional[dict] = Depends(
        authenticator.get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    return {"trips": queries.get_user_trips(account_data["username"])}


@router.get("/api/trips/{trip_id}", response_model=TripOut)
def get_trip(
    trip_id: str,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    if account_data:
        trip = queries.get_one_by_id(trip_id=trip_id)
        if trip is None or trip["author"] != account_data["username"]:
            raise HTTPException(status_code=404, detail="trip not found")
        else:
            return trip
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )


@router.put("/api/trips/{trip_id}", response_model=TripOut)
def update_trip(
    trip_id: str,
    trip_in: TripIn,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    if account_data:
        trip = queries.get_one_by_id(trip_id=trip_id)
        if trip is None or trip["author"] != account_data["username"]:
            raise HTTPException(status_code=404, detail="trip not found")
        else:
            updated_trip = queries.update(
                trip_id=trip_id,
                trip_in=trip_in,
                author=account_data["username"],
            )
            return updated_trip
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )


@router.delete("/api/trips/{trip_id}", response_model=DeleteStatus)
def delete_trip(
    trip_id: str,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    print("****" * 100)

    if account_data:
        trip = queries.get_one_by_id(trip_id=trip_id)
        if trip is None or trip["author"] != account_data["username"]:
            raise HTTPException(status_code=404, detail="trip not found")
        else:
            response = {"deleted": queries.delete(trip_id=trip_id)}
            print(response)
            return response
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )


@router.put("/api/trips/{trip_id}/stop", response_model=TripOut)
def add_stop(
    trip_id: str,
    stop_in: StopIn,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
    mapbox_queries: MapboxQueries = Depends(),
):
    if account_data:
        trip = queries.get_one_by_id(trip_id=trip_id)
        if trip["author"] != account_data["username"]:
            raise HTTPException(status_code=404, detail="trip not found")
        else:
            stop_in = dict(stop_in)
            stop_coords = mapbox_queries.geocode(address=stop_in["address"])
            stop_in["coordinates"] = stop_coords
            stop_in["p_id"] = len(trip["stops"])
            trip["stops"].append(stop_in)
            updated_trip = queries.update(
                trip_id=trip_id,
                trip_in=trip,
                author=account_data["username"],
            )
            return updated_trip
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )


@router.put("/api/trips/{trip_id}/stop/delete", response_model=TripOut)
def remove_stop(
    trip_id: str,
    p_id: int,
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
    queries: TripQueries = Depends(),
):
    if account_data:
        trip = queries.get_one_by_id(trip_id=trip_id)
        if trip["author"] != account_data["username"]:
            raise HTTPException(status_code=404, detail="trip not found")
        else:
            for stop in trip["stops"]:
                if p_id == stop["p_id"]:
                    trip["stops"].remove(stop)
            for stop in trip["stops"]:
                if p_id <= stop["p_id"]:
                    stop["p_id"] -= 1
            updated_trip = queries.update(
                trip_id=trip_id, trip_in=trip, author=account_data["username"]
            )
            return updated_trip
    else:
        raise HTTPException(
            status_code=401,
            detail="You must be logged in to access this feature.",
        )
