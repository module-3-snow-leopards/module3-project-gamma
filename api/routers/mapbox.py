from fastapi import APIRouter, Depends, HTTPException
from models.mapbox import (
    SearchIn,
    GeocodeOut,
    SearchOutList,
    RouteOut,
)
from queries.mapbox import MapboxQueries
from queries.trips import TripQueries

router = APIRouter()


@router.get("/api/location/{address}", response_model=GeocodeOut)
def geocode(
    address: str,
    queries: MapboxQueries = Depends(),
):
    response = queries.geocode(address=address)
    return response


@router.post("/api/location/search", response_model=SearchOutList)
def search(
    search_in: SearchIn,
    queries: MapboxQueries = Depends(),
):
    search_in = search_in.dict()
    response = queries.search_location(search_in)
    result = {}
    sub_result = []
    for feature in response["features"]:
        sub_result.append(
            {
                "place_name": feature["place_name"],
                "text": feature["text"],
                # needed to be switched to match what mapbox marker wants
                # "latitude": feature["center"][0],
                # "longitude": feature["center"][1],
                "longitude": feature["center"][0],
                "latitude": feature["center"][1],
            }
        )

    result["places"] = sub_result
    return SearchOutList(**result)


@router.post("/api/{trip_id}/directions", response_model=RouteOut)
def directions(
    trip_id: str,
    queries: MapboxQueries = Depends(),
    trip_queries: TripQueries = Depends(),
):
    trip = trip_queries.get_one_by_id(trip_id=trip_id)
    if len(trip["stops"]) < 2:
        raise HTTPException(
            status_code=400, detail="The trip is not long enough"
        )
    route = queries.get_directions(trip=trip)

    return route
