from fastapi.testclient import TestClient
from main import app
from queries.trips import TripQueries
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {
        "username": "fake_user",
        "email": "fake@email.com",
        "password": "password",
        "full_name": "Fake User",
    }


class FakeTripQueries:
    def get_all_trips(self):
        return [
            {
                "id": "64af05c670c281e4be764f94",
                "name": "fake_name",
                "author": "fake_user",
                "description": "string",
                "is_public": False,
                "days": 0,
                "is_completed": False,
                "rating": 0,
                "stops": [],
            },
        ]

    def get_one_by_id(self, trip_id: str):
        return {
            "id": trip_id,
            "name": "fake_name",
            "author": "fake_user",
            "description": "string",
            "is_public": False,
            "days": 0,
            "is_completed": False,
            "rating": 0,
            "stops": [],
        }

    def delete(self, trip_id: str):
        return True


def test_list_all_trips():
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/api/trips")
    data = res.json()

    assert data == {
        "trips": [
            {
                "id": "64af05c670c281e4be764f94",
                "name": "fake_name",
                "author": "fake_user",
                "description": "string",
                "is_public": False,
                "days": 0,
                "is_completed": False,
                "rating": 0,
                "stops": [],
            },
        ]
    }


def test_get_trip():
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    trip_id = "64af05c670c281e4be764f94"
    res = client.get(f"/api/trips/{ trip_id }")
    data = res.json()

    assert data == {
        "id": trip_id,
        "name": "fake_name",
        "author": "fake_user",
        "description": "string",
        "is_public": False,
        "days": 0,
        "is_completed": False,
        "rating": 0,
        "stops": [],
    }


def test_delete_trip():
    trip_id = "64af05c670c281e4be764f94"
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data

    response = client.delete(f"/api/trips/{trip_id}")
    assert response.status_code == 200
    assert response.json() == {"deleted": True}
