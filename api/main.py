from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import os
from authenticator import authenticator
from routers import accounts, mapbox, trips


app = FastAPI()
app.include_router(authenticator.router, tags=["Authenticate"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(trips.router, tags=["Trips"])
app.include_router(mapbox.router, tags=["Mapbox"])


@app.get("/")
def home():
    return True


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:3000",
        os.environ.get("CORS_HOST", None)],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
