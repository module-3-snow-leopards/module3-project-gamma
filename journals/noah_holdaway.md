## DAY 1 (6/26/23)
I made a git group and forked the project for the team. We each made branches to explore the repository and set up our yaml files for mongo.
The team is functioning well together. Everyone is communicative and contributing to the project.


## DAY 2 (6/27/23)
We worked through getting user authentication running. There were a few hick ups, mostly resolved by paying closer attention to the types of data being exchanged, making sure the type hints match all of the promised data from functions. We spent a bit of time today figuring out how to use mongo, which is currently working. We plan to get proper routes and queries set for a trip model tomorrow.


## DAY 3 (6/28/23)
Today we were able to get CRUD routes and queries for Trips, as well as make simply requests over to the mapbox API for geocoding information.


## DAY 4 (6/29/23)
Mostly cleaned up code today and used the authenticator on different routes for the trip. Timothee hasn't contributed to the code base yet so I asked him to try to write out a function for the mapbox routes while I walked him through the process. He had to leave, left the function unfinished and pushed to main.


## DAY 5 (6/30/23)
We are almost finished with the routes, both for the trips and mapbox. I'm nervous about taking a full week off right after we started getting momentum with the project.

## DAY -1 (8/1/23)
I just noticed that our pipeline failed and I couldn't get into contact with Dane so I am making a commit to get a working pipeline.
